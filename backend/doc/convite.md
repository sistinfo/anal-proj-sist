# **Convite**

## Cadastrar um convite
* URL: /invite
* Método: POST
* Body:
    * user: Email do usuário para enviar o convite
    * group: ID do grupo
* Retorno:
    * success: indica sucesso da operação
* *Retorna 400 se o user já estiver em um grupo*

## Retornar convites de um usuário
* URL: /invite?id=*<id_do_usuario>*
* Método: GET
* Retorno:
    * invites: Lista de grupos que o usuário foi convidado (`[]` se o usuário não recebeu nenhum convite)
        * id: ID do invite
        * user: Email do usuário que foi convidado (mesmo do URL)
        * group:
            * name: Nome do grupo
            * description: Descrição do grupo
            * id: ID do grupo
            * users: Lista de emails dos usuários do grupo
    * success: indica sucesso da operação

## Deletar um convite
* URL: /invite?id=<id_do_convite>
* Método: DELETE
* Retorno:
    * success: indica sucesso da operação
