# **Despesa**

## Cadastrar uma despesa
* URL: /expense
* Método: POST
* Body:
    * owner: Email do usuário que criou a despesa
    * name: Nome da despesa
    * description: Descrição da despesa
    * cost: Custo (valor double, não string)
    * time: Quando (valor long, não string)
* Retorno:
    * success: indica sucesso da operação

## Retornar despesas de um usuário
* URL: /expense?id=*<id_do_usuario>*
* Método: GET
* Retorno:
    * expenses: Lista de despesas
        * id: ID da despesa
        * name: Nome da despesa
        * owner: Email do usuário que criou a despesa
        * description: Descrição da despesa
        * cost: Custo (valor double, não string)
        * time: Quando foi realizado (valor long, não string)
    * success: indica sucesso da operação

## Alterar nome, descrição e gasto de uma despesa
* URL: /expense
* Método: PUT
* Body:
    * id: ID da despesa
    * name: Nome
    * description: Descrição
    * cost: Custo (valor double, não string)
* Retorno:
    * success: indica sucesso da operação

## Deletar uma despesa
* URL: /expense?id=<id_da_despesa>
* Método: DELETE
* Retorno:
    * success: indica sucesso da operação
