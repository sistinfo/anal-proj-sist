# **Grupo**

## Cadastrar um grupo
* URL: /group
* Método: POST
* Body:
    * name: Nome
    * description: Descrição
    * creator: Email de quem criou
* Retorno:
    * success: indica sucesso da operação

## Retornar informações de um grupo
* URL: /group?id=*<id_do_grupo>*
* Método: GET
* Retorno:
    * group: Informações do Grupo
        * name: Nome
        * description: Descrição
        * creator: Email de quem criou o grupo
        * users: Lista de emails de usuários presentes no grupo
    * users: Lista de usuários presentes no grupo
        * id: Email do usuário
        * name: Nome do usuário
    * success: indica sucesso da operação

## Retornar despesas de membros de um grupo
* URL: /group/expenses?id=*<id_do_grupo>*
* Método: GET
* Retorno:
    * expenses: Lista com despesas
        * _id: ID da despesa
        * name: Nome da despesa
        * owner: Quem criou
        * description: Descrição da despesa
        * cost: Custo (valor double, não string)
        * time: Quando foi realizado (valor long, não string)
        * ownerInfo: Informações do *owner*
            * name: Nome do usuário
            * id: Email do usuário
    * success: indica sucesso da operação

## Alterar nome do grupo
* URL: /group
* Método: PUT
* Body:
    * name: Nome
    * id: ID do grupo
* Retorno:
    * success: indica sucesso da operação

## Cadastra um usuário em um grupo
* URL: /group/user
* Método: POST
* Body:
    * user: Email do usuário
    * group: ID do grupo
* Retorno:
    * success: indica se a senha está correta

## Retorna grupo de um usuário
* URL: /group/user
* Método: GET
* Retorno:
    * group:
        * id: ID do grupo
        * name: Nome do grupo
        * description: Descrição do grupo
        * users: Lista de emails dos usuários do grupo
    * success: indica se a senha está correta

## Remove uma pessoa de um grupo
* URL: /group/user?id=*<id_do_usuário>*
* Método: DELETE
* Retorno:
    * success: indica se a senha está correta

## Alterar nome e descrição do grupo
* URL: /group
* Método: PUT
* Body:
    * id: ID do grupo
    * name: Nome
    * description: Descrição
* Retorno:
    * success: indica se a senha está correta

## Deletar um grupo
* URL: /group?id=*<id_do_grupo>*
* Método: DELETE
* Retorno:
    * success: indica sucesso da operação
