# **Usuário**

## Cadastrar usuário
* URL: /user
* Método: POST
* Body:
    * name: Nome
    * id: Email
    * password: Senha
* Retorno:
    * success: indica sucesso da operação

## Retornar informações do usuário
* URL: /user?id=*<id_do_usuário>*
* Método: GET
* Retorno:
    * user: Informações do usuário
        * id: Email
        * name: Nome
        * group: ID do grupo (se estiver em um)
    * success: indica sucesso da operação

## Alterar nome do usuário
* URL: /user
* Método: PUT
* Body:
    * name: Nome
    * id: Email
* Retorno:
    * success: indica sucesso da operação

## Verificar senha de usuário
* URL: /user/password
* Método: POST
* Body:
    * id: Email
    * password: Senha
* Retorno:
    * success: indica se a senha está correta

## Alterar senha de usuário
* URL: /user/password
* Método: PUT
* Body:
    * id: Email
    * password: Nova senha
* Retorno:
    * success: indica sucesso da operação

## Deletar um usuário
* URL: /user?id=<id_do_usuário>
* Método: DELETE
* Retorno:
    * success: indica sucesso da operação
