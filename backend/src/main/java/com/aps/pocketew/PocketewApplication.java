package com.aps.pocketew;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocketewApplication {

    public static void main(String[] args) throws IOException, InterruptedException {
        if(args.length > 0 && args[0].equals("restart")){
            SpringApplication.run(PocketewApplication.class, args);
        }else{
            System.out.println("Executando com Xmx200M.");
            String wkdir = new File("./").getAbsolutePath().replace(".", "");
            String jarPath = System.getProperty("java.class.path");
            String currentJarPath = wkdir + jarPath;

            ProcessBuilder pb = new ProcessBuilder(
                                        "java",
                                        "-Xmx200M",
                                        "-jar",
                                        currentJarPath,
                                        "restart");
            Process p = pb.start();
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String s = "";
            while((s = in.readLine()) != null){
                System.out.println(s);
            }
            int status = p.waitFor();
            System.out.println("Exited with status: " + status);
        }

	}

}
