package com.aps.pocketew.controller;

import java.util.List;

import com.aps.pocketew.dao.ExpenseDAO;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExpenseController {

    private ExpenseDAO expenseDao = new ExpenseDAO();

    /*
        Retorna dados de todas as despesas de uma entidade
        /expense?id=<id_de_entidade>
    */
    @GetMapping(value="/expense", produces="application/json")
    public ResponseEntity<String> getExpense(@RequestParam(value="id") String id) {
        List<Document> result = expenseDao.getAll(id);
        
        JSONObject response = new JSONObject();
        response = response.put("success", true)
                            .put("expenses", result);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(response.toString());
    }

    /*
        Cria uma despesa
        body: { "owner": <id_do_usuário>, "name": "Gasolina", "description": "Ta caro", "cost": 8.00, "time": <unix_time> }
    */
    @PostMapping(value="/expense", produces="application/json", consumes="application/json")
    public ResponseEntity<String> createExpense(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = expenseDao.create(
                            json.getString("name"),
                            json.getString("description"),
                            json.getString("owner"),
                            json.getDouble("cost"),
                            json.getLong("time")
                        );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }
    }

    /*
        Atualiza: nome, descricao, gasto
        body: { "id": <id_da_despesa>, "name": "Petroleo", "description": "Ta muito caro", "cost": 1000000 }
    */
    @PutMapping(value="/expense", produces="application/json", consumes="application/json")
    public ResponseEntity<String> updateExpense(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = expenseDao.update(
                            json.getString("id"),
                            json.getString("name"),
                            json.getString("description"),
                            json.getDouble("cost")
                        );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Deleta um gasto
        /expense?id=<id_do_gasto>
    */
    @DeleteMapping(value="/expense", produces="application/json")
    public ResponseEntity<String> deleteExpense(@RequestParam(value="id") String id) {
        Integer result;
        JSONObject response = new JSONObject();

        try {
            result = expenseDao.delete(id);
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }
        
        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

}