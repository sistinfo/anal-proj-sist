package com.aps.pocketew.controller;

import java.util.ArrayList;
import java.util.List;

import com.aps.pocketew.dao.GroupDAO;
import com.aps.pocketew.dao.UserDAO;
import com.aps.pocketew.models.Group;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController {

    private UserDAO userDao = new UserDAO();
    private GroupDAO groupDao = new GroupDAO();

    /*
        Retorna informações de um grupo
        /group?id=<id_do_grupo>
    */
    @GetMapping(value="/group", produces="application/json")
    public ResponseEntity<String> getGroup(@RequestParam(value="id") String id) {
        Group group = groupDao.get(id);

        JSONObject response = new JSONObject();
        if(group != null){
            List<Document> users = new ArrayList<Document>();
    
            for(String u : group.getUsers()){
                users.add(userDao.get(u).toSecureDocument());
            }
            
            response = response.put("success", true)
                               .put("group", group.toDocument())
                               .put("users", users);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Retorna despesas de usuários de um grupo
        /group/expenses?id=<id_do_grupo>
    */
    @GetMapping(value="/group/expenses", produces="application/json")
    public ResponseEntity<String> getGroupExpenses(@RequestParam(value="id") String id) {
        List<Document> expenses = groupDao.getGroupExpenses(id);

        JSONObject response = new JSONObject();
        if(expenses != null){
            response = response.put("success", true)
                               .put("expenses", expenses);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Cadastra um grupo
        body: { "name": "YMCA", "description": "isso mesmo", "creator": <id_de_quem_criou> }
    */
    @PostMapping(value="/group", produces="application/json", consumes="application/json")
    public ResponseEntity<String> createGroup(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = groupDao.create(
                                json.getString("name"),
                                json.getString("description"),
                                json.getString("creator")
                            );  
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }      

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }
    }

    /*
        Cadastra uma pessoa num grupo
        body: { "group": <id_do_grupo>, "user": <id_do_usuario> }
    */
    @PostMapping(value="/group/user", produces="application/json", consumes="application/json")
    public ResponseEntity<String> userJoin(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = groupDao.insertUser(
                                json.getString("group"),
                                json.getString("user")
                            );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }
                
        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else if(result == 2){
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }


    /*
        Retorna informações do grupo de um usuário
        /group/user?id=<id_do_usuario>
    */
    @GetMapping(value="/group/user", produces="application/json")
    public ResponseEntity<String> getUserGroup(@RequestParam(value="id") String id) {
        JSONObject response = new JSONObject();
        Group group = groupDao.getUserGroup(id);

        if(group != null){
            response = response.put("success", true)
                               .put("group", group.toDocument());
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Retira uma pessoa de um grupo
        /group/user?id=<id_do_usuario>
    */
    @DeleteMapping(value="/group/user", produces="application/json", consumes="application/json")
    public ResponseEntity<String> userRemove(@RequestParam(value="id") String id) {
        JSONObject response = new JSONObject();
        Integer result = 0;

        result = groupDao.removeUser(id);
        
        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else if(result == 2){
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

    /*
        Atualiza: nome, descrição
        body: { "id": <id_grupo>, "name": "AIA", "description": "grupo top" }
    */
    @PutMapping(value="/group", produces="application/json", consumes="application/json")
    public ResponseEntity<String> updateGroup(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = groupDao.update(
                                json.getString("id"),
                                json.getString("name"),
                                json.getString("description")
                            );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }
        
        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else if(result == 2){
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

    /*
        Deleta um grupo
        /group?id=<id_do_grupo>
    */
    @DeleteMapping(value="/group", produces="application/json")
    public ResponseEntity<String> deleteGroup(@RequestParam(value="id") String id) {
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = groupDao.delete(id);
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }
        
        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

}