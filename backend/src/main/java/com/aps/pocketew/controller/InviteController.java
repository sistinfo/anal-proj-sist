package com.aps.pocketew.controller;

import java.util.List;

import com.aps.pocketew.dao.InviteDAO;
import com.aps.pocketew.models.Invite;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InviteController {

    private InviteDAO inviteDao = new InviteDAO();

    /*
        Retorna convites de um usuário
        /invites?id=<id_do_usuario>
    */
    @GetMapping(value="/invites", produces="application/json")
    public ResponseEntity<String> getUser(@RequestParam(value="id") String id) {
        List<Invite> result = inviteDao.getAll(id);
        JSONObject response = new JSONObject();

        if(result != null){
            response = response.put("success", true)
                               .put("invites", result);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Cadastra um convite
        body: { "user": <id_do_usuario>, "group": <id_do_grupo> }
    */
    @PostMapping(value="/invites", produces="application/json", consumes="application/json")
    public ResponseEntity<String> createUser(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = inviteDao.create(
                                json.getString("user"),
                                json.getString("group")
                            );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

    /*
        Deleta um convite
        /invite?id=<id_do_convite>
    */
    @DeleteMapping(value="/invites", produces="application/json")
    public ResponseEntity<String> deleteUser(@RequestParam(value="id") String id) {
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = inviteDao.delete(id);
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

}