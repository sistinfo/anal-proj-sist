package com.aps.pocketew.controller;

import com.aps.pocketew.dao.UserDAO;
import com.aps.pocketew.models.User;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private UserDAO userDao = new UserDAO();

    /*
        Retorna informações da pessoa
        /user?id=<id_do_usuario>
    */
    @GetMapping(value="/user", produces="application/json")
    public ResponseEntity<String> getUser(@RequestParam(value="id") String id) {
        User user = userDao.get(id);
        JSONObject response = new JSONObject();

        if(user != null){
            response = response.put("success", true)
                               .put("user", user.toSecureDocument());
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Cadastra um usuário
        body: { "name": "Joao Silva", "id": "jao", "password": "senha123" }
    */
    @PostMapping(value="/user", produces="application/json", consumes="application/json")
    public ResponseEntity<String> createUser(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = userDao.create(
                                json.getString("id"),
                                json.getString("password"),
                                json.getString("name")
                            );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

    /*
        Atualiza: nome
        body: { "id": "jao", "name": "Joao Silveira" }
    */
    @PutMapping(value="/user", produces="application/json", consumes="application/json")
    public ResponseEntity<String> updateUser(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = userDao.update(
                                json.getString("id"),
                                json.getString("name")
                             );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        }
    }

    /*
        Verifica senha
        body: { "id": "jao", "password": "senha123" }
    */
    @PostMapping(value="/user/password", produces="application/json", consumes="application/json")
    public ResponseEntity<String> checkUserPassword(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = userDao.checkPassword(
                                json.getString("id"),
                                json.getString("password")
                            );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else if(result == 0){
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.UNAUTHORIZED)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Atualiza senha
        body: { "id": "jao", "password": "senha1234" }
    */
    @PutMapping(value="/user/password", produces="application/json", consumes="application/json")
    public ResponseEntity<String> updateUserPassword(HttpEntity<String> httpEntity) {
        JSONObject json = new JSONObject(httpEntity.getBody());
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = userDao.updatePassword(
                                json.getString("id"),
                                json.getString("password")
                            );
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

    /*
        Deleta uma pessoa
        /user?id=<id_do_usuario>
    */
    @DeleteMapping(value="/user", produces="application/json")
    public ResponseEntity<String> deleteUser(@RequestParam(value="id") String id) {
        JSONObject response = new JSONObject();
        Integer result = 0;

        try {
            result = userDao.delete(id);
        } catch (JSONException e) {
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(response.toString());
        }

        if(result == 1){
            response = response.put("success", true);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(response.toString());
        }else{
            response = response.put("success", false);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(response.toString());
        }
    }

}