package com.aps.pocketew.dao;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

public abstract class DAO {

    private MongoClientURI mongoURI = new MongoClientURI("mongodb+srv://aps_user:NLUDF2xwRURXzeG@aps-rl4e8.mongodb.net/admin?retryWrites=true&w=majority");
    //private MongoClientURI mongoURI = new MongoClientURI("mongodb://0.0.0.0:32768");
    private MongoClient mongoClient = new MongoClient(mongoURI);
    private MongoDatabase database = mongoClient.getDatabase("pocketdb");
    protected MongoCollection<Document> collection;

    // Construtor, realiza a configuração inicial
    protected MongoCollection<Document> createCollectionIfNotExists(String collectionName) {
        if(!collectionsExists(collectionName))
            database.createCollection(collectionName);
        return database.getCollection(collectionName);
    }

    // Verifica se uma collection existe
    private boolean collectionsExists(String collectionName) {
        MongoIterable<String> collection =  database.listCollectionNames();
        for(String s : collection) {
            if(s.equals(collectionName)) {
                return true;
            }
        }
        return false;
    }
}
