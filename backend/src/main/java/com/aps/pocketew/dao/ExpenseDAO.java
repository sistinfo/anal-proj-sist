package com.aps.pocketew.dao;

import java.util.ArrayList;
import java.util.List;

import com.aps.pocketew.models.Expense;
import com.mongodb.client.FindIterable;

import org.bson.Document;
import org.bson.types.ObjectId;

public class ExpenseDAO extends DAO {

    private UserDAO userDao = new UserDAO();

    // Construtor, realiza a configuração inicial
    public ExpenseDAO() {
        collection = createCollectionIfNotExists("expenses");
    }

    // Verifica se um id existe
    public Boolean exists(String id){
        FindIterable<Document> iterable = collection.find(new Document("_id",  new ObjectId(id)));
        return iterable.first() != null;
    }

    // Retorna um gasto específico
    public Expense get(String id) {
        if(!exists(id)) return null;
        
        Document query = new Document("_id",  new ObjectId(id));
        Document result = collection.find(query).first();
        return new Expense(result);
    }

    // Retorna uma lista de gastos de algum usuário
    public List<Document> getAll(String user){
        List<Document> result = new ArrayList<Document>();

        FindIterable<Document> iterable = collection.find(new Document("owner", user));
        if(iterable.first() == null) return result;

        for(Document doc : iterable){
            Document eDoc = new Expense(doc).toDocument();
            eDoc.append("ownerInfo", userDao.get(user).toSecureDocument());
            result.add(eDoc);
        }

        return result;
    }

    // Cria um novo gasto
    /* Retorna
        1: Sucesso
    */
    public Integer create(String name, String description, String owner, Double cost, Long time){
        Expense expense = new Expense(name, owner, description, cost, time);

        Document document = expense.toDocument();
        collection.insertOne(document);

        return 1;
    }

    // Atualiza informações de um gasto
    /* Retorna
        1: Sucesso
        2: Gasto não existe
    */
    public Integer update(String id, String name, String description, Double cost){
        if(!exists(id)) return 2;

        Document docId = new Document("_id", new ObjectId(id));
        Expense expense = new Expense( collection.find(docId).first() );
        
        expense.setName(name);
        expense.setDescription(description);
        expense.setCost(cost);

        Document update = new Document("$set", expense.toDocument());
        collection.findOneAndUpdate(docId, update);

        return 1;
    }

    // Remove um gasto
    /* Retorna
        1: Sucesso
    */
    public Integer delete(String id){
        if(!exists(id)) return 1;

        Document query = new Document("_id", new ObjectId(id));
        collection.findOneAndDelete(query);

        return 1;
    }
}
