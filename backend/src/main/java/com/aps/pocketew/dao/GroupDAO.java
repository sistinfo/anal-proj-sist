package com.aps.pocketew.dao;

import java.util.ArrayList;
import java.util.List;

import com.aps.pocketew.models.Group;
import com.mongodb.client.FindIterable;

import org.bson.Document;
import org.bson.types.ObjectId;

public class GroupDAO extends DAO {

    // Construtor, realiza a configuração inicial
    public GroupDAO() {
        collection = createCollectionIfNotExists("groups");
    }

    // Verifica se o grupo existe
    public Boolean exists(String id){
        FindIterable<Document> iterable = collection.find(new Document("_id", new ObjectId(id)));
        return iterable.first() != null;
    }

    // Retorna um grupo
    public Group get(String id) {
        if(!exists(id)) return null;

        Document query = new Document("_id",  new ObjectId(id));
        Document result = collection.find(query).first();
        return new Group(result);
    }

    // Retorna grupo que um usuário está, null se não estiver em nenhum grupo
    public Group getUserGroup(String user){
        FindIterable<Document> iterable = collection.find(); // Lista de documentos de grupos
        if(iterable.first() == null) return null;

        for(Document doc : iterable){
            List<String> users = doc.getList("users", String.class);
            for(String u : users){
                if(u.equals(user)){
                    return new Group(doc);
                }
            }
        }

        return null;
    }

    // Retorna lista de despesas de um grupo
    public List<Document> getGroupExpenses(String id){
        List<Document> result = new ArrayList<Document>();
        ExpenseDAO expenseDao = new ExpenseDAO();
        Group group = get(id);
        
        for(String u : group.getUsers()){ // ID dos usuários
            for(Document e : expenseDao.getAll(u)){ // Despesas do usuário (u)
                result.add(e);
            }
        }
        
        return result;
    }
    
    // Cria um grupo
    /* Retorna
        1: Sucesso
    */
    public Integer create(String name, String description, String creator){
        Group group = new Group(name, description, creator);
        group.addUser(creator);

        Document document = group.toDocument();
        collection.insertOne(document);

        syncCreatorGroup(creator);
        return 1;
    }

    // Sincroniza as informações do usuário com a do grupo
    private void syncCreatorGroup(String userid){
        Group group = getUserGroup(userid);
        UserDAO userDao = new UserDAO();
        userDao.updateGroup(userid, group.getId());
    }
    
    // Atualiza perfil
    /* Retorna
        1: Sucesso
        2: Grupo não existe
    */
    public Integer update(String id, String name, String description) {
        if(!exists(id)) return 2;

        Group group = get(id);
        group.setName(name);
        group.setDescription(description);

        Document query = new Document("_id",  new ObjectId(id));
        Document document = group.toDocument();
        Document update = new Document("$set", document);
        collection.updateOne(query, update);

        return 1;
    }

    // Insere usuário num grupo
    /* Retorna
        1: Sucesso
        2: Grupo não existe
    */
    public Integer insertUser(String id, String userId) {
        if(!exists(id)) return 2;
        InviteDAO inviteDao = new InviteDAO();

        UserDAO userDao = new UserDAO();
        userDao.updateGroup(userId, id);

        Group group = get(id);
        group.addUser(userId);

        inviteDao.deleteAll(userId);

        Document query = new Document("_id",  new ObjectId(id));
        Document document = group.toDocument();
        Document update = new Document("$set", document);
        collection.updateOne(query, update);

        return 1;
    }

    // Remove usuário de um grupo
    /* Retorna
        1: Sucesso
    */
    public Integer removeUser(String user){
        UserDAO userDao = new UserDAO();
        userDao.updateGroup(user, null);

        Group group = getUserGroup(user);

        group.removeUser(user);

        if(group.getUsers().size() == 0){
            delete(group.getId());
        }else{
            Document query = new Document("_id",  new ObjectId(group.getId()));
            Document document = group.toDocument();
            Document update = new Document("$set", document);
            collection.updateOne(query, update);
        }

        return 1;
    }

    // Deleta um grupo
    /* Retorna
        1: Sucesso
    */
    public Integer delete(String id){
        if(!exists(id)) return 1;
        UserDAO userDao = new UserDAO();

        Group group = get(id);
        for(String u : group.getUsers()){
            userDao.updateGroup(u, null);
        }

        Document query = new Document("_id",  new ObjectId(id));
        collection.findOneAndDelete(query);

        return 1;
    }
    
}
