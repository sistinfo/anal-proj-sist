package com.aps.pocketew.dao;

import java.util.ArrayList;
import java.util.List;

import com.aps.pocketew.models.Group;
import com.aps.pocketew.models.Invite;
import com.mongodb.client.FindIterable;

import org.bson.Document;
import org.bson.types.ObjectId;

public class InviteDAO extends DAO {

    private UserDAO userDao = new UserDAO();

    // Construtor, realiza a configuração inicial
    public InviteDAO() {
        collection = createCollectionIfNotExists("invites");
    }

    // Verifica se um convite existe
    public Boolean exists(String id){
        FindIterable<Document> iterable = collection.find(new Document("_id",  new ObjectId(id)));
        return iterable.first() != null;
    }

    // Retorna um convite específico
    public Invite get(String id) {
        if(!exists(id)) return null;
        
        Document query = new Document("_id",  new ObjectId(id));
        Document result = collection.find(query).first();
        return new Invite(result);
    }

    // Retorna uma lista de grupos que um usuário já foi convidado
    public List<Invite> getAll(String user){
        List<Invite> result = new ArrayList<Invite>();

        FindIterable<Document> iterable = collection.find(new Document("user", user));
        if(iterable.first() == null) return result;

        for(Document doc : iterable){
            //result.add(new Group(doc.get("group", Document.class)));
            result.add(new Invite(doc));
        }

        return result;
    }

    // Remove todos os convites de um usuário
    /* Retorna
        1: Sucesso
    */
    public Integer deleteAll(String user){
        FindIterable<Document> iterable = collection.find(new Document("user", user));
        if(iterable.first() == null) return 1;

        for(Document doc : iterable){
            Invite invite = new Invite(doc);
            delete(invite.getId());
        }

        return 1;
    }

    // Cria um novo convite
    /* Retorna
        1: Sucesso
        2: Usuário já está em um grupo
    */
    public Integer create(String user, String groupId){
        if(userDao.get(user).getGroup() != null) return 2;
        GroupDAO groupDao = new GroupDAO();
        Group group = groupDao.get(groupId);

        Invite invite = new Invite(user, group);

        Document document = invite.toDocument();
        collection.insertOne(document);

        return 1;
    }

    // Remove um convite
    /* Retorna
        1: Sucesso
    */
    public Integer delete(String id){
        if(!exists(id)) return 1;

        Document query = new Document("_id", new ObjectId(id));
        collection.findOneAndDelete(query);

        return 1;
    }
}
