package com.aps.pocketew.dao;

import com.aps.pocketew.models.User;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import org.bson.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class UserDAO extends DAO {

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    private MongoCollection<Document> logsCollection;

    // Construtor, realiza a configuração inicial
    public UserDAO() {
        collection = createCollectionIfNotExists("users");
        logsCollection = createCollectionIfNotExists("login_logs");
    }

    // Verifica se usuário existe
    public boolean exists(String id){
        FindIterable<Document> iterable = collection.find(new Document("id", id));
        return iterable.first() != null;
    }

    public User get(String id) {
        if(!exists(id)) return null;

        Document query = new Document("id", id);
        Document result = collection.find(query).first();
        return new User(result);
    }
    
    // Cria usuário
    /* Retorna
        1: Sucesso
        2: Usuário já existe
    */
    public Integer create(String id, String password, String name){
        if(exists(id)) return 2;
        password = encoder.encode(password);

        User user = new User(id, password, name, 0);
        Document document = user.toDocument();
        collection.insertOne(document);

        return 1;
    }
    
    // Atualiza perfil
    /* Retorna
        1: Sucesso
        2: Usuário não existe
    */
    public Integer update(String id, String name) {
        if(!exists(id)) return 2;

        User user = get(id);
        Document query = new Document("id", id);

        user.setName(name);
        Document document = user.toDocument();

        Document update = new Document("$set", document);
        collection.updateOne(query, update);

        return 1;
    }

    // Atualiza id do grupo
    /* Retorna
        1: Sucesso
        2: Usuário não existe
    */
    public Integer updateGroup(String id, String groupId) {
        if(!exists(id)) return 2;

        User user = get(id);
        Document query = new Document("id", id);

        user.setGroup(groupId);
        Document document = user.toDocument();

        Document update = new Document("$set", document);
        collection.updateOne(query, update);

        return 1;
    }

    // Atualiza senha
    /* Retorna
        1: Sucesso
        2: Usuário não existe
    */
    public Integer updatePassword(String id, String password) {
        if(!exists(id)) return 2;
        password = encoder.encode(password);

        User user = get(id);
        user.setPassword(password);

        Document query = new Document("id", id);
        Document update = new Document("$set", user.toDocument());

        collection.findOneAndUpdate(query, update);

        return 1;
    }

    // Retorna true se a senha for compatível com a do usuário
    /* Retorna
        0: Não autorizado
        1: Sucesso
        2: Usuário não existe
    */
    public Integer checkPassword(String id, String password){
        if(!exists(id)) return 2;

        Document log = new Document("user", id);
        User user = get(id);
        if(encoder.matches(password, user.getPassword())){
            log.append("success", true);
            logsCollection.insertOne(log);
            return 1;
        }
        
        log.append("success", false);
        logsCollection.insertOne(log);
        return 0;
    }

    // Remove um usuário
    /* Retorna
        1: Sucesso
        2: Usuário não existe
    */
    public Integer delete(String id){
        if(!exists(id)) return 2;
        
        User user = get(id);
        user.clearExpenses();

        if(user.getGroup() != null){
            GroupDAO groupDao = new GroupDAO();
            groupDao.removeUser(user.getId());
        }

        Document query = new Document("id", id);
        collection.findOneAndDelete(query);

        return 1;
    }
    
}
