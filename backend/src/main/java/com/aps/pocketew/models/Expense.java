package com.aps.pocketew.models;

import org.bson.Document;

public class Expense implements Model {
    
    private String id;
    private String name;
    private String description;
    private String owner;
    private Double cost;
    private Long time;

    public Expense( String name,
                    String owner,
                    String description,
                    Double cost,
                    Long time ){
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.cost = cost;
        this.time = time;
    }

    public Expense( Document doc ){
        this.id = doc.getObjectId("_id").toString();
        this.name = doc.getString("name");
        this.owner = doc.getString("owner");
        this.description = doc.getString("description");
        this.cost = doc.getDouble("cost");
        this.time = doc.getLong("time");
    }

    /* GETs */

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public String getOwner(){
        return owner;
    }

    public Double getCost(){
        return cost;
    }

    public Long getTime(){
        return time;
    }

    /* SETs */

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setCost(Double cost){
        this.cost = cost;
    }

    /* Methods */

    public Document toDocument(){
        Document doc = new Document();
        doc.append("id", id);
        doc.append("name", name);
        doc.append("description", description);
        doc.append("owner", owner);
        doc.append("cost", cost);
        doc.append("time", time);

        return doc;
    }
}