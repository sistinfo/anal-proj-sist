package com.aps.pocketew.models;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

public class Group implements Model {

    private String id;
    private String name;
    private String description;
    private String creator;
    private List<String> users;

    public Group(String name,
                 String description,
                 String creator){
        this.name = name;
        this.description = description;
        this.creator = creator;
        this.users = new ArrayList<String>();
    }

    public Group(Document document){
        this.id = document.getObjectId("_id").toString();
        this.name = document.getString("name");
        this.description = document.getString("description");
        this.creator = document.getString("creator");
        this.users = document.getList("users", String.class);
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public String getCreator(){
        return creator;
    }

    public List<String> getUsers(){
        return users;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public void setCreator(String creator){
        this.creator = creator;
    }

    public void addUser(String user){
        users.add(user);
    }

    public void removeUser(String user){
        users.remove(user);
    }

    /* Methods */
    
    public Document toDocument(){
        Document doc = new Document();
        doc.append("id", id);
        doc.append("name", name);
        doc.append("description", description);
        doc.append("users", users);
        doc.append("creator", creator);

        return doc;
    }
}