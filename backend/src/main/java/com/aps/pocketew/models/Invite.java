package com.aps.pocketew.models;

import org.bson.Document;

public class Invite implements Model {
    
    private String id;
    private String name;
    private String user;
    private Document group;

    public Invite( String user,
                   Group group){
        this.user = user;
        this.group = group.toDocument();
    }

    public Invite( Document doc ){
        this.id = doc.getObjectId("_id").toString();
        this.user = doc.getString("user");
        this.group = doc.get("group", Document.class);
    }

    /* GETs */

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getUser(){
        return user;
    }

    public Document getGroup(){
        return group;
    }

    /* SETs */

    public void setName(String name){
        this.name = name;
    }

    public void setUser(String userId){
        this.user = userId;
    }

    public void setGroup(Group groupId){
        this.group = groupId.toDocument();
    }

    /* Methods */

    public Document toDocument(){
        Document doc = new Document();
        doc.append("user", user);
        doc.append("group", group);

        return doc;
    }
}