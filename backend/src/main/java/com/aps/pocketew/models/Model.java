package com.aps.pocketew.models;

import org.bson.Document;

public interface Model {

    public Document toDocument();

}