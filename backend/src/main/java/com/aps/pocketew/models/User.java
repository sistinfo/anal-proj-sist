package com.aps.pocketew.models;

import java.util.List;

import com.aps.pocketew.dao.ExpenseDAO;

import org.bson.Document;

public class User implements Model {

    private String id;
    private String name;
    private String password;
    private String group;

    public User(String id,
                String password,
                String name,
                Integer spent){
        this.id = id;
        this.password = password;
        this.name = name;
        this.group = null;
    }

    public User(Document document){
        this.id = document.getString("id");
        this.password = document.getString("password");
        this.name = document.getString("name");
        this.group = document.getString("group");
    }

    /* GETs */

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getPassword(){
        return password;
    }

    public String getGroup(){
        return group;
    }

    /* SETs */

    public void setId(String id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setGroup(String group){
        this.group = group;
    }

    /* Methods */
    
    public Document toDocument(){
        Document doc = new Document();
        doc.append("id", id);
        doc.append("name", name);
        doc.append("password", password);
        doc.append("group", group);

        return doc;
    }

    // Passwordless
    public Document toSecureDocument(){
        Document doc = toDocument();
        doc.remove("password");
        return doc;
    }

    public void clearExpenses(){
        ExpenseDAO eDao = new ExpenseDAO();
        List<Document> expenses = eDao.getAll(id);

        if(expenses.size() == 0) return;

        for(Document expense : expenses){
            Expense exp = new Expense(expense);
            eDao.delete(exp.getId());
        }
    }
}