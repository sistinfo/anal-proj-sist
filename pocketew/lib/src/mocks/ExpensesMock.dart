import 'package:pocketew/src/models/Expense.dart';
import 'package:pocketew/src/models/TypeExpense.dart';

class ExpensesMock {
  static Future<List<Expense>> getAll({ TypeExpense type = TypeExpense.PERSONAL }) async {
    final List<Expense> personal = [
      Expense(
        id: '5d9df0691300c6f8c59f949a',
        name: 'Compras Mercado',
        cost: 321.65,
        time: 1570537800,
        owner: "eu@ele.com",
      ),
      Expense(
        id: '5d9df1185116557799ff154d',
        name: 'Compras da Farmácia',
        description: 'Dorflex',
        cost: 23.31,
        time: 1570631938,
        owner: "eu@ele.com",
      ),
      Expense(
        id: '5d9df11cd0fcaeb016e8626d',
        name: 'Almoço no Centro',
        cost: 11.97,
        time: 1570634406,
        owner: "eu@ele.com",
      ),
      Expense(
        id: '5d9df1200d5116932b3d5537',
        name: 'Compra Banca',
        description: 'Cigarros e chiclete',
        cost: 14.25,
        time: 1570537800,
        owner: "eu@ele.com",
      ),
    ];
    if (type == TypeExpense.GROUP) {
      personal.addAll([
        Expense(
          id: '5d9df36fd6bfa3158cb0871f',
          name: 'Compras Shopping',
          cost: 543.15,
          time: 1570472406,
          owner: "mae@ele.com",
        ),
        Expense(
          id: '5d9df378d983ecb6c644a785',
          name: 'Compras da Farmácia',
          description: 'Desodorante e chiclete',
          cost: 17.14,
          time: 1570631938,
          owner: "mae@ele.com",
        ),
        Expense(
          id: '5d9df37e439db7630d7ebceb',
          name: 'Almoço no Centro',
          cost: 15.97,
          time: 1570634406,
          owner: "pai@ele.com",
        ),
        Expense(
          id: '5d9df3897e7f3abd9ff286a0',
          name: 'Jantar no Shopping',
          cost: 14.25,
          time: 1570537800,
          owner: "pai@ele.com",
        ),
      ]);
    }
    personal.sort((Expense a, Expense b) {
      if (a.time < b.time) return 1;
      else if (a.time > b.time) return -1;
      else return 0;
    });
    return personal;
  }
}