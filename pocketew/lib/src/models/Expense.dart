import 'package:pocketew/src/models/User.dart';

class Expense {
  final String id;
  final String name;
  final String owner;
  final User ownerInfo;
  final String description;
  final num cost;
  final int time;

  Expense({ this.id, this.name, this.owner, this.description, this.cost, this.time, this.ownerInfo });

  factory Expense.fromJson(Map<String, dynamic> json) {
    final ownerInfo = User.fromJson(json['ownerInfo']);
    return Expense(
      id: json['id'],
      name: json['name'],
      owner: json['owner'],
      description: json['description'],
      cost: json['cost'],
      time: json['time'],
      ownerInfo: ownerInfo,
    );
  }

  Map<String, dynamic> toJson() => ({
    'id': this?.id,
    'name': this?.name,
    'owner': this?.owner,
    'description': this?.description,
    'cost': this?.cost,
    'time': this?.time,
  });
}