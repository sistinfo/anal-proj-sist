import 'package:pocketew/src/models/User.dart';

class Group {
  final String id;
  final String name;
  final String creator;
  final String description;
  final List<User> users;

  Group({ this.creator, this.id, this.name, this.description, this.users });

  factory Group.fromJson(Map<String, dynamic> json) {
    final List<User> users = [];
    json['users'].forEach((user) {
      if (user is String) {
        users.add(User(id: user));
      } else {
        users.add(User.fromJson((user)));
      }
    });
    return Group(
      id: json['id'],
      name: json['name'],
      creator: json['creator'],
      description: json['description'],
      users: users,
    );
  }

  Map<String, dynamic> toJson() => ({
    'id': this.id,
    'creator': this?.creator,
    'name': this?.name,
    'description': this?.description,
  });
}