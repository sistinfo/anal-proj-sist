import 'package:pocketew/src/models/Group.dart';

class Invite {
  final String id;
  final String user;
  final Group group;

  Invite({ this.id, this.user, this.group });

  factory Invite.fromJson(Map<String, dynamic> json) {
    print(json['group']);
    return Invite(
      id: json['id'],
      user: json['user'],
      group: Group.fromJson(json['group']),
    );
  }

  Map<String, dynamic> toJson() => ({
    'user': this?.user,
    'group': this.group.id,
  });
}