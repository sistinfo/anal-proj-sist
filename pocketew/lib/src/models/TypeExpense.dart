enum TypeExpense {
  GROUP,
  PERSONAL,
}

const Map<TypeExpense, String> TYPE_EXPENSE_MAP = {
  TypeExpense.GROUP: 'group',
  TypeExpense.PERSONAL: 'personal',
};