class User {
  final String id;
  final String name;
  final String password;
  final String idGroup;

  User({ this.id, this.name, this.password, this.idGroup });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['name'],
      idGroup: json['group'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': this?.name,
      'id': this?.id,
      'password': this?.password,
    };
  }
}