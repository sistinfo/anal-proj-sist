import 'dart:convert';

import 'package:http/http.dart' as http;

class ApiProvider {
  final String baseApi = 'https://pocketew.herokuapp.com/';
  final Map<String, String> headers = {
    'Content-Type': 'application/json',
  };

  Future<http.Response> get(String path) {
    return http.get(
      baseApi + path,
      headers: headers,
    );
  }

  Future<http.Response> post(String path, Map body) {
    print(json.encode(body));
    return http.post(
      baseApi + path,
      headers: headers,
      body: json.encode(body),
    );
  }

  Future<http.Response> put(String path, Map body) {
    print(json.encode(body));
    return http.put(
      baseApi + path,
      headers: headers,
      body: json.encode(body),
    );
  }

  Future<http.Response> delete(String path) {
    print(path);
    return http.delete(
      baseApi + path,
      headers: headers,
    );
  }
}
