import 'dart:convert';
import 'package:pocketew/src/mocks/ExpensesMock.dart';
import 'package:pocketew/src/models/Expense.dart';
import 'package:pocketew/src/models/TypeExpense.dart';
import 'package:pocketew/src/resources/ApiProvider.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ExpensesService {
  final bool mock;
  final api = ApiProvider();
  ExpensesService({ this.mock = false });

  Future<List<Expense>> getAll(String id, { TypeExpense type = TypeExpense.PERSONAL, bool reverse = false }) async {
    if (mock) {
      return ExpensesMock.getAll(type: type);
    }
    String url;
    if (type == TypeExpense.GROUP) {
      url = 'group/expenses?id=$id';
    } else {
      url = 'expense?id=$id';
    }
    final response = await api.get(url);
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == true) {
      final expensesJson = body['expenses'];
      final List<Expense> expenses = [];
      expensesJson.forEach((json) {
        expenses.add(Expense.fromJson((json)));
      });
      if (reverse) {
        return expenses.reversed.toList(growable: false);
      }
      return expenses;
    }
    throw Exception('Falha ao carregar despesas.');
  }

  Future<void> create(Expense expense) async {
    final response = await api.post('expense', expense.toJson());
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao cadastrar despesa.');
    }
  }

  Future<void> delete(String id) async {
    final response = await api.delete('expense?id=$id');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao remover despesa.');
    }
  }

  Future<void> update(Expense expense) async {
    final response = await api.put('expense', expense.toJson());
    final body = json.decode(response.body);
    print(response.statusCode);
    if (body['success'] == false) {
      throw Exception('Falha ao atualizar despesa.');
    }
  }

  Future<List<charts.Series<AccumulatedExpenses, DateTime>>> monthlyExpenses(String id, { TypeExpense type = TypeExpense.PERSONAL }) async {
    List<Expense> expenses = await getAll(id, type: type);
    final now = DateTime.now().add(Duration(days: 1));
    final List<AccumulatedExpenses> accumulatedExpenses = [];
    var date = DateTime.now().subtract(Duration(days: 30));
    final expensesFilter = expenses
        .where((exp) => exp.time > date.millisecondsSinceEpoch).toList(growable: true);

    while (date.isAtSameMomentAs(now) || date.isBefore(now)) {
      accumulatedExpenses.add(AccumulatedExpenses(
        expenses: expensesFilter.where((expense) {
          final expenseDate = DateTime.fromMillisecondsSinceEpoch(expense.time);
          return expenseDate.day == date.day &&
              expenseDate.month == date.month &&
              expenseDate.year == date.year;
        }).toList(growable: true),
        date: date,
      ));
      date = date.add(Duration(days: 1));
    }

    return [
      charts.Series<AccumulatedExpenses, DateTime>(
        id: 'Despesas',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (AccumulatedExpenses accExpenses, _) => accExpenses.date,
        measureFn: (AccumulatedExpenses accExpenses, _) => accExpenses.totalCost,
        data: accumulatedExpenses,
      ),
    ];
  }

  Future<List<charts.Series<AccumulatedExpenses, DateTime>>> yearlyExpenses(String id, { TypeExpense type = TypeExpense.PERSONAL }) async {
    List<Expense> expenses = await getAll(id, type: type);
    final now = DateTime.now();
    final List<AccumulatedExpenses> accumulatedExpenses = [];
    var date = now.subtract(Duration(days: 360));
    final expensesFilter = expenses
        .where((exp) => exp.time > date.millisecondsSinceEpoch).toList(growable: true);

    while (date.isAtSameMomentAs(now) || date.isBefore(now)) {
      accumulatedExpenses.add(AccumulatedExpenses(
        expenses: expensesFilter.where((expense) {
          final expenseDate = DateTime.fromMillisecondsSinceEpoch(expense.time);
          return expenseDate.month == date.month &&
              expenseDate.year == date.year;
        }).toList(growable: true),
        date: date,
      ));
      date = date.add(Duration(days: 30));
    }

    return [
      charts.Series<AccumulatedExpenses, DateTime>(
        id: 'Despesas',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (AccumulatedExpenses accExpenses, _) => accExpenses.date,
        measureFn: (AccumulatedExpenses accExpenses, _) => accExpenses.totalCost,
        data: accumulatedExpenses,
      ),
    ];
  }
}

class AccumulatedExpenses {
  final DateTime date;
  final List<Expense> expenses;
  double totalCost;

  AccumulatedExpenses({ this.date, this.expenses }) {
    totalCost = expenses.fold(0.0, (acc, expense) {
      return acc + expense.cost;
    });
  }
}
