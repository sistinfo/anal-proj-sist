import 'dart:convert';
import 'package:pocketew/src/models/Group.dart';
import 'package:pocketew/src/resources/ApiProvider.dart';

class GroupsService {
  final api = ApiProvider();

  Future<Group> get(String idGroup) async {
    final response = await api.get('group?id=$idGroup');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == true) {
      body['group']['users'] = body['users'];
      return Group.fromJson(body['group']);
    }
    throw Exception('Falha ao retornar informações do grupo.');
  }

  Future<void> create(Group group) async {
    final response = await api.post('group', group.toJson());
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao cadastrar grupo.');
    }
  }

  Future<void> delete(String id) async {
    final response = await api.delete('group?id=$id');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao remover grupo.');
    }
  }

  Future<void> removeFromGroup(String idUser) async {
    final response = await api.delete('group/user?id=$idUser');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao remover do grupo.');
    }
  }

  Future<void> update(Group group) async {
    final response = await api.put('group', group.toJson());
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao atualizar grupo.');
    }
  }
}