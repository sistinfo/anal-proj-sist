import 'package:flutter/material.dart';

void showAlertDialog(String msg, BuildContext context) {
  showDialog(
    context:  context,
    builder:  (BuildContext context) {
      return AlertDialog(
        title: Text(msg),
      );
    },
  );
}

void showLoadingDialog(BuildContext context) {
  showDialog(
    barrierDismissible: false,
    context:  context,
    builder:  (BuildContext context) {
      return AlertDialog(
        content: Image.asset('assets/loading.gif'),
      );
    },
  );
}
