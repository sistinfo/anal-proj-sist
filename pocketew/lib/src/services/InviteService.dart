import 'dart:convert';
import 'package:pocketew/src/models/Invite.dart';
import 'package:pocketew/src/resources/ApiProvider.dart';

class InvitesService {
  final api = ApiProvider();

  Future<void> acceptInvite(Invite invite) async {
    final response = await api.post('group/user', invite.toJson());
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao retornar informações do grupo.');
    }
  }

  Future<void> create(String email, String group) async {
    final response = await api.post('invites', {
      'user': email,
      'group': group,
    });
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao cadastrar convite.');
    }
  }

  Future<void> delete(String id) async {
    final response = await api.delete('invites?id=$id');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao remover convite.');
    }
  }

  Future<List<Invite>> getAll(String idUser) async {
    final response = await api.get('invites?id=$idUser');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == true) {
      final invitesJson = body['invites'];
      final List<Invite> invites = [];
      invitesJson.forEach((json) {
        invites.add(Invite.fromJson((json)));
      });
      return invites;
    }
    throw Exception('Falha ao carregar convites.');
  }
}