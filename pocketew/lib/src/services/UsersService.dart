import 'dart:convert';
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/resources/ApiProvider.dart';

class UsersService {
  final api = ApiProvider();

  Future<User> get(String idUser) async {
    final response = await api.get('user?id=$idUser');
    final body = json.decode(response.body);
    if (body['success'] == true) {
      print(json.decode(response.body));
      return User.fromJson(json.decode(response.body)['user']);
    }
    throw Exception('Falha ao retornar informações do usuário.');
  }

  Future<void> create(User user) async {
    final response = await api.post('user', user.toJson());
    final body = json.decode(response.body);
    if (body['success'] == false) {
      throw Exception('Falha ao cadastrar usuário.');
    }
  }

  Future<void> validatePassword(User user) async {
    final response = await api.post('user/password', user.toJson());
    final body = json.decode(response.body);
    if (body['success'] == false) {
      throw Exception('Falha ao validar senha.');
    }
  }

  Future<void> delete(String id) async {
    final response = await api.delete('user?id=$id');
    final body = json.decode(response.body);
    print(body);
    if (body['success'] == false) {
      throw Exception('Falha ao remover usuário.');
    }
  }

  Future<void> update(User user) async {
    if (user.password.isNotEmpty) {
      final responsePw = await api.put('user/password', user.toJson());
      final bodyPw = json.decode(responsePw.body);
      if (bodyPw['success'] == false) {
        throw Exception('Falha ao atualizar senha do usuário.');
      }
    }
    final response = await api.put('user', user.toJson());
    final body = json.decode(response.body);
    if (body['success'] == false) {
      throw Exception('Falha ao atualizar usuário.');
    }
  }
}