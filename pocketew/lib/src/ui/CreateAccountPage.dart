import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/services/Helpers.dart';
import 'package:pocketew/src/services/UsersService.dart';
import 'package:pocketew/src/ui/MainPage.dart';

class CreateAccountPage extends StatefulWidget {
  CreateAccountPage({ Key key }) : super(key : key);
  final usersService = UsersService();
  @override
  _CreateAccountPageState createState() => _CreateAccountPageState();
}

class _CreateAccountPageState extends State<CreateAccountPage> {
  TextStyle _style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final nameField = TextFormField(
      controller: _nameController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Nome',
      ),
      validator: (value) {
        if (value.length <= 3) {
          return 'O nome deve ter mais de 3 caracteres';
        }
        return null;
      },
    );
    final emailField = TextFormField(
      controller: _emailController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Email',
      ),
      validator: (value) {
        bool emailValid = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
        if (!emailValid) {
          return 'Entre com um endereço de email válido!';
        }
        return null;
      },
    );
    final passwordField = TextFormField(
      controller: _passwordController,
      obscureText: true,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Senha',
      ),
      validator: (value) {
        if (value.length < 6) {
          return 'A senha deve ter no mínimo 6 caracteres';
        }
        return null;
      },
    );
    final createAccountButton = Material(
      elevation: 5.0,
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            try {
              showLoadingDialog(context);
              await widget.usersService.create(User(
                name: _nameController.text,
                password: _passwordController.text,
                id: _emailController.text,
              ));
              try {
                final user = await widget.usersService.get(_emailController.text);
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (ctxt) => MainPage(user: user)));
              } catch(error) {
                Navigator.pop(context);
                showAlertDialog('Não foi possível encontrar o usuário.', context);
              }
            } catch(error) {
              print(error);
              Navigator.pop(context);
              showAlertDialog('Não foi possível criar a conta.', context);
            }
          }
        },
        child: Text('Cadastrar',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Cadastre-se'),
      ),
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: nameField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: emailField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: passwordField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30.0),
                  child: createAccountButton,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}