import 'package:flutter/material.dart';
import 'package:pocketew/src/models/Group.dart';
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/services/GroupsService.dart';
import 'package:pocketew/src/services/Helpers.dart';

class GroupInfoPage extends StatefulWidget {
  final User user;
  final onUserChange;
  final groupsService = GroupsService();
  GroupInfoPage ({ Key key, @required this.user, this.onUserChange }) : super(key : key);


  @override
  _GroupInfoPageState  createState() => _GroupInfoPageState();
}

class _GroupInfoPageState extends State<GroupInfoPage> {
  TextStyle _style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();
  final _nameController = TextEditingController();
  bool firstTime = true;

  @override
  Widget build(BuildContext context) {
    final nameField = TextFormField(
      controller: _nameController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Nome',
      ),
      validator: (value) {
        if (value.length <= 3) {
          return 'O nome deve ter mais de 3 caracteres';
        }
        return null;
      },
    );
    final descriptionField = TextFormField(
      controller: _descriptionController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Descrição',
      ),
    );
    final List<Widget> children = [
      Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        child: nameField,
      ),
      Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        child: descriptionField,
      ),
    ];
    if (widget.user.idGroup != null && widget.user.idGroup.isNotEmpty) {
      return FutureBuilder(
        future: widget.groupsService.get(widget.user.idGroup),
        builder: (BuildContext ctxt, AsyncSnapshot<Group> snap) {
          if (snap.hasData) {
            if (firstTime) {
              _descriptionController.text = snap.data.description;
              _nameController.text = snap.data.name;
              firstTime = false;
            }

            final updateGroupButton = Material(
              elevation: 5.0,
              color: Color(0xff01A0C7),
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      showLoadingDialog(context);
                      await widget.groupsService.update(Group(
                        id: snap.data.id,
                        name: _nameController.text,
                        description: _descriptionController.text,
                      ));
                      Navigator.pop(context);
                      showAlertDialog('Sucesso!', context);
                    } catch (error) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      showAlertDialog('Não foi possível atualizar o grupo!', context);
                    }
                  }
                },
                child: Text('Atualizar',
                    textAlign: TextAlign.center,
                    style: _style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)
                ),
              ),
            );
            final deleteGroupButton = Material(
              elevation: 5.0,
              color: Colors.red,
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () async {
                  try {
                    showLoadingDialog(context);
                    await widget.groupsService.delete(widget.user.idGroup);
                    if (widget.onUserChange != null) {
                      widget.onUserChange();
                    }
                    Navigator.pop(context);
                    Navigator.pop(context);
                    showAlertDialog('Sucesso!', context);
                  } catch (error) {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    showAlertDialog('Não foi possível remover o grupo!', context);
                  }
                },
                child: Text('Remover Grupo',
                    textAlign: TextAlign.center,
                    style: _style.copyWith(
                        color: Colors.white, fontWeight: FontWeight.bold)
                ),
              ),
            );
            children.addAll([
              Container(
                margin: EdgeInsets.symmetric(vertical: 30.0),
                child: UsersListView(group: snap.data, user: widget.user, groupsService: widget.groupsService, onUserChange: widget.onUserChange),
              ),
            ]);
            if (widget.user.id == snap.data.creator) {
              children.addAll([
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30.0),
                  child: updateGroupButton,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 0.0),
                  child: deleteGroupButton,
                ),
              ]);
            }
            return Scaffold(
              appBar: AppBar(
                title: Text('Alterar informações do grupo'),
              ),
              body: Container(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      children: children,
                    ),
                  ),
                ),
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: Image.asset('assets/loading.gif'),
              ),
            );
          }
        },
      );
    }
    final createGroupButton = Material(
      elevation: 5.0,
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            try {
              showLoadingDialog(context);
              await widget.groupsService.create(Group(
                name: _nameController.text,
                description: _descriptionController.text,
                creator: widget.user.id,
              ));
              if (widget.onUserChange != null) {
                widget.onUserChange();
              }
              Navigator.pop(context);
              Navigator.pop(context);
              showAlertDialog('Sucesso!', context);
            } catch(error) {
              print(error);
              Navigator.pop(context);
              Navigator.pop(context);
              showAlertDialog('Não foi possível criar grupo.', context);
            }
          }
        },
        child: Text('Cadastrar Grupo',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );
    children.addAll([
      Container(
        margin: EdgeInsets.symmetric(vertical: 30.0),
        child: createGroupButton,
      ),
    ]);
    return Scaffold(
      appBar: AppBar(
        title: Text('Criar grupo'),
      ),
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: children,
            ),
          ),
        ),
      ),
    );
  }
}

class UsersListView extends StatefulWidget {
  final GroupsService groupsService;
  final onUserChange;
  final Group group;
  final User user;
  final style = TextStyle(
    fontSize: 18,
  );

  UsersListView({ this.user, this.group, this.groupsService, this.onUserChange });


  @override
  _UserListViewState createState() => _UserListViewState();
}

class _UserListViewState extends State<UsersListView> {
  Widget createItem(BuildContext context, User user, TextStyle style) {
    return ListTile(
      leading: widget.group.creator == user.id ? Icon(Icons.grade) : Icon(Icons.perm_identity),
      title: Text(
        user.name,
        style: style,
      ),
      subtitle: Text(user.id),
      trailing: widget.group.creator == widget.user.id || widget.user.id == user.id ?
      GestureDetector(
        onTap: () async {
          try {
            showLoadingDialog(context);
            await widget.groupsService.removeFromGroup(user.id);
            setState(() {
              widget.group.users.removeWhere((usr) => usr.id == user.id);
            });
            if (user.id == widget.user.id) {
              if (widget.onUserChange != null) {
                widget.onUserChange();
              }
              Navigator.pop(context);
            }
            Navigator.pop(context);
            showAlertDialog('Sucesso!', context);
          } catch(error) {
            Navigator.pop(context);
            showAlertDialog('Não foi possível remover do grupo!', context);
          }
        },
        child: Icon(Icons.cancel),
      ) :
      SizedBox(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: widget.group.users.length,
      itemBuilder: (BuildContext itemCtxt, index) => createItem(context, widget.group.users[index], widget.style),
    );
  }
}