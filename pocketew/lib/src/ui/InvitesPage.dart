import 'package:flutter/material.dart';
import 'package:pocketew/src/models/Invite.dart';
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/services/Helpers.dart';
import 'package:pocketew/src/services/InviteService.dart';

class InvitesPage extends StatelessWidget {
  final User user;
  final onUserChange;
  InvitesPage ({ Key key, this.user, this.onUserChange }) : super(key : key);

  @override
  Widget build(BuildContext context) {
    Widget child;
    if (user.idGroup == null) {
      child = InvitesListView(
        user: user,
        onUserChange: onUserChange,
      );
    } else {
      child = InviteForm(user: user);
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Convites'),
      ),
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: child,
        ),
      ),
    );
  }
}

class InviteForm extends StatefulWidget {
  final invitesService = InvitesService();
  final User user;

  InviteForm({ this.user });

  @override
  _InviteForm createState() => _InviteForm();
}

class _InviteForm extends State<InviteForm> {
  final TextStyle _style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final _emailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 50.0),
        child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 40),
              child: TextFormField(
                style: _style,
                obscureText: false,
                controller: _emailController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  hintText: 'Email',
                ),
                validator: (value) {
                  bool emailValid = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
                  if (!emailValid) {
                    return 'Entre com um endereço de email válido!';
                  }
                  return null;
                },
              ),
            ),
            Material(
              elevation: 5.0,
              color: Color(0xff01A0C7),
              child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      showLoadingDialog(context);
                      await widget.invitesService.create(_emailController.text, widget.user.idGroup);
                      _emailController.text = '';
                      Navigator.pop(context);
                      showAlertDialog('Sucesso!', context);
                    } catch (error) {
                      Navigator.pop(context);
                      showAlertDialog('Não foi possível enviar o convite', context);
                    }
                  }
                },
                child: Text('Enviar Convite',
                  textAlign: TextAlign.center,
                  style: _style.copyWith(
                    color: Colors.white, fontWeight: FontWeight.bold)
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class InvitesListView extends StatefulWidget {
  final onUserChange;
  final User user;
  final invitesService = InvitesService();
  final style = TextStyle(
    fontSize: 18,
  );

  InvitesListView({ this.user, this.onUserChange });

  @override
  _InvitesListViewState createState() => _InvitesListViewState();
}



class _InvitesListViewState extends State<InvitesListView> {
  Future<List<Invite>> invites;


  void initState() {
    super.initState();
    invites = widget.invitesService.getAll(widget.user.id);
  }

  void reloadInvites() {
    setState(() {
      invites = widget.invitesService.getAll(widget.user.id);
    });
  }

  Widget createItem(BuildContext context, Invite invite, TextStyle style) {
    return ListTile(
      title: Text(
        invite.group.name,
        style: style,
      ),
      trailing: SizedBox(
        width: 100,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            GestureDetector(
              onTap: () async {
                try {
                  showLoadingDialog(context);
                  await widget.invitesService.acceptInvite(invite);
                  if (widget.onUserChange != null) {
                    widget.onUserChange();
                  }
                  Navigator.pop(context);
                  Navigator.pop(context);
                  showAlertDialog('Sucesso!', context);
                } catch(error) {
                  Navigator.pop(context);
                  showAlertDialog('Não foi possível aceitar o convite', context);
                }
              },
              child: Container(
                margin: EdgeInsets.only(right: 25),
                child: Icon(
                  Icons.check,
                  size: 30,
                  color: Colors.green,
                ),
              ),
            ),
            GestureDetector(
              onTap: () async {
                try {
                  showLoadingDialog(context);
                  await widget.invitesService.delete(invite.id);
                  reloadInvites();
                  Navigator.pop(context);
                } catch(error) {
                  showAlertDialog('Não foi possível recusar o convite', context);
                }
              },
              child: Container(
                child: Icon(
                  Icons.cancel,
                  size: 30,
                  color: Colors.red,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: invites,
      builder: (BuildContext context, AsyncSnapshot<List<Invite>> snap) {
        print(snap.data);
        if (snap.hasData) {
          if (snap.data.isEmpty) {
            return Center(
              child: Text(
                'Não há convites!',
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            );
          }
          return ListView.builder(
            itemCount: snap.data.length,
            itemBuilder: (BuildContext itemCtxt, index) => createItem(context, snap.data[index], widget.style),
          );
        } else {
          return Center(
            child: Image.asset('assets/loading.gif'),
          );
        }
      },
    );
  }
}
