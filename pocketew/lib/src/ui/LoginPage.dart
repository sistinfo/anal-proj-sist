import 'package:flutter/material.dart';
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/services/Helpers.dart';
import 'package:pocketew/src/services/UsersService.dart';
import 'package:pocketew/src/ui/CreateAccountPage.dart';
import 'MainPage.dart';

class LoginPage extends StatefulWidget {
  LoginPage({ Key key }) : super(key : key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle _style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final _formKey = GlobalKey<FormState>();
  final _usersService = UsersService();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final logo = Text(
      'Pocketew',
      textAlign: TextAlign.center,
      style: _style.copyWith(
          fontSize: 36,
          color: Colors.black,
          fontWeight: FontWeight.bold,
      ),
    );
    final emailField = TextFormField(
      controller: _emailController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: 'Email',
      ),
      validator: (value) {
        bool emailValid = RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value);
        if (!emailValid) {
          return 'Entre com um endereço de email válido!';
        }
        return null;
      },
    );
    final passwordField = TextFormField(
      controller: _passwordController,
      obscureText: true,
      style: _style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: 'Senha',
      ),
      validator: (value) {
        if (value.length < 6) {
          return 'A senha deve ter no mínimo 6 caracteres';
        }
        return null;
      },
    );
    final loginButton = Material(
      elevation: 5.0,
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            showLoadingDialog(context);
            final userTest = User(
              id: _emailController.text,
              password: _passwordController.text,
            );
            try {
              await _usersService.validatePassword(userTest);
              try {
                final user = await _usersService.get(userTest.id);
                print(user.id);
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MainPage(user: user))
                );
              } catch (error) {
                Navigator.pop(context);
                showAlertDialog('Não foi possível buscar informações do usuário!', context);
              }
            } catch (error) {
              print(error);
              Navigator.pop(context);
              showAlertDialog('Email ou senha incorretos!', context);
            }
          }
        },
        child: Text('Login',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );
    final createAccountButton = Material(
      elevation: 5.0,
      color: Colors.green,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateAccountPage())
          );
        },
        child: Text('Criar Conta',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );

    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 50.0),
                  child: logo,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: emailField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: passwordField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30.0),
                  child: loginButton,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 0.0),
                  child: createAccountButton,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}