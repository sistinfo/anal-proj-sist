import 'package:flutter/material.dart';
import 'package:pocketew/src/models/Group.dart';
import 'package:pocketew/src/models/Invite.dart';
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/services/UsersService.dart';
import 'package:pocketew/src/ui/GroupInfoPage.dart';
import 'package:pocketew/src/ui/InvitesPage.dart';
import 'package:pocketew/src/ui/MainPageChildren/ReportsWidget.dart';
import 'package:pocketew/src/ui/UserInfoPage.dart';
import 'MainPageChildren/ExpensesWidget.dart';
import 'MainPageChildren/HistoryWidget.dart';
import '../models/TypeExpense.dart';

class MainPage extends StatefulWidget {
  final User user;
  final usersService = UsersService();
  MainPage({ this.user });

  @override
  _MainPageState createState() =>  _MainPageState(user: user);
}

class _MainPageState extends State<MainPage> {
  User user;
  _MainPageState({ this.user });

  void onUserChange() async {
    try {
      final newUser = await widget.usersService.get(user.id);
      setState(() {
        user = newUser;
      });
    } catch(err) {
      print(err);
    }

  }

  @override
  Widget build(BuildContext context) {
    print(user.id);
    return WillPopScope(
      onWillPop: () async => false,
      child: InheritedUserState(
        user: user,
        child: DefaultTabController(
          length: 3,
          child: Scaffold(
            drawer: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  DrawerHeader(
                    child: Center(
                        child:Text(
                          'Pocketew',
                          style: TextStyle(fontSize: 36),
                        )
                    ),
                    decoration: BoxDecoration(
                        color: Colors.blue
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text('Informações Pessoais'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => UserInfoPage(
                            onUserChange: onUserChange,
                            user: user,
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.group),
                    title: Text('Informações de seus Grupos'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => GroupInfoPage(
                            onUserChange: onUserChange,
                            user: user,
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.group_add),
                    title: Text('Convites'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => InvitesPage(
                            onUserChange: onUserChange,
                            user: user,
                          ),
                        ),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.power_settings_new),
                    title: Text('Sair'),
                    onTap: () {
                      Navigator.of(context).popUntil((route) => route.isFirst);
                    },
                  ),
                ],
              ),
            ),
            appBar: AppBar(
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: const Icon(Icons.menu),
                    onPressed: ()  => Scaffold.of(context).openDrawer(),
                    tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                  );
                },
              ),
              bottom: TabBar(
                tabs: [
                  Tab(
                    icon: Icon(Icons.attach_money),
                    text: 'Despesas',
                  ),
                  Tab(
                    icon: Icon(Icons.history),
                    text: 'Histórico',
                  ),
                  Tab(
                    icon: Icon(Icons.show_chart),
                    text: 'Relatórios',
                  ),
                ],
              ),
              title: Text('Pocketew'),
            ),
            body: TabBarView(
              children: [
                ExpensesWidget(),
                Container(
                  child: PersonalOrGroupTabBarController(
                    personal: HistoryWidget(type: TypeExpense.PERSONAL),
                    group: HistoryWidget(type: TypeExpense.GROUP),
                  ),
                ),
                Container(
                  child: PersonalOrGroupTabBarController(
                    personal: ReportsWidget(type: TypeExpense.PERSONAL),
                    group: ReportsWidget(type: TypeExpense.GROUP),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PersonalOrGroupTabBarController extends DefaultTabController {
  PersonalOrGroupTabBarController({ Key key, Widget group, Widget personal }) : super(
    key: key,
    length: 2,
    child: Column(
      children: <Widget>[
        Container(
          child: TabBar(
            labelColor: Colors.black,
            tabs: [
              Tab(text: 'Pessoal'),
              Tab(text: 'Grupo'),
            ],
          ),
        ),
        Expanded(
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              personal,
              group,
            ],
          ),
        ),
      ],
    ),
  );
}

class InheritedUserState extends InheritedWidget {
  final User user;
  final onUserChange;

  InheritedUserState({
    this.user,
    Widget child,
    this.onUserChange,
  }) : super(child: child);

  @override
  bool updateShouldNotify(InheritedUserState oldWidget) {
    return user?.id != oldWidget.user?.id ||
           onUserChange != oldWidget.onUserChange;
  }

  static InheritedUserState of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(InheritedUserState);
  }
}