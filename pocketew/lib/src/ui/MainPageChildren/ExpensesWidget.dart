import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:pocketew/src/models/Expense.dart';
import 'package:pocketew/src/services/ExpensesService.dart';
import 'package:pocketew/src/services/Helpers.dart';
import 'package:pocketew/src/ui/MainPage.dart';



class ExpensesWidget extends StatelessWidget {
  ExpensesWidget({ Key key }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Text(
            'Cadastre uma despesa:',
            style: TextStyle(
              fontSize: 24,
            ),
          ),
        ),
        ExpensesForm(),
      ],
    );
  }
}

class ExpensesForm extends StatefulWidget {
  final buttonText;
  final showRemove;
  final Expense value;
  final expensesService = ExpensesService();
  final user;

  ExpensesForm({ this.value, this.buttonText = 'Cadastrar', this.showRemove = false, this.user });

  @override
  _ExpensesFormState createState() => _ExpensesFormState();
}

class _ExpensesFormState extends State<ExpensesForm> {
  final _formKey = GlobalKey<FormState>();

  MoneyMaskedTextController _costController;
  TextEditingController _nameController;
  TextEditingController _descriptionController;

  TextStyle _style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  void initState() {
    super.initState();
    _costController = MoneyMaskedTextController(
      leftSymbol: 'R\$ ',
      initialValue: widget.value != null ? widget.value.cost.toDouble() : 0.00,
    );
    _nameController = TextEditingController(
      text: widget.value != null ? widget.value.name : '',
    );
    _descriptionController = TextEditingController(
      text: widget.value != null ? widget.value.description : '',
    );
  }

  @override
  Widget build(BuildContext context) {
    final nameField = TextFormField(
      controller: _nameController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Nome',
      ),
      validator: (value) {
        if (value.length < 3) {
          return 'O nome deve ter no mínimo 3 caracteres!';
        }
        return null;
      },
    );
    final descriptionField = TextFormField(
      controller: _descriptionController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Descrição',
      ),
    );
//    _costController.updateValue(widget.value?.cost);
    final costField = TextFormField(
      controller: _costController,
      obscureText: false,
      style: _style,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Custo',
      ),
      validator: (value) {
        if (_costController.numberValue <= 0.0) {
          return 'Insira um custo válido';
        }
        return null;
      },
    );

    final createExpenseButton = Material(
      elevation: 5.0,
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            final owner = widget.user == null ?
              InheritedUserState.of(context).user.id : widget.user.id;
            final expense = Expense(
              id: widget?.value?.id,
              name: _nameController.text,
              description: _descriptionController.text,
              cost: _costController.numberValue,
              time: widget.buttonText == 'Cadastrar' ? DateTime.now().millisecondsSinceEpoch : widget.value.time,
              owner: owner,
            );
            showLoadingDialog(context);
            if (widget.buttonText == 'Cadastrar') {
              try {
                await widget.expensesService.create(expense);
                Navigator.pop(context);
                showAlertDialog('Sucesso!', context);
              } catch(error) {
                Navigator.pop(context);
                showAlertDialog('Não foi possível cadastrar a despesa.', context);
              }
            } else {
              try {
                await widget.expensesService.update(expense);
                Navigator.pop(context);
                Navigator.pop(context);
                showAlertDialog('Sucesso!', context);
              } catch(error) {
                Navigator.pop(context);
                showAlertDialog('Não foi possível atualizar a despesa.', context);
              }
            }
          }
        },
        child: Text(widget.buttonText,
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );

    final removeExpenseButton = Material(
      elevation: 5.0,
      color: Colors.red,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          try {
            showLoadingDialog(context);
            await widget.expensesService.delete(widget.value.id);
            Navigator.pop(context);
            Navigator.pop(context);
            showAlertDialog('Sucesso!', context);
          } catch (error) {
            Navigator.pop(context);
            Navigator.pop(context);
            showAlertDialog('Não foi possível remover a despesa.', context);
          }
        },
        child: Text('Remover',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );

    final children = <Widget> [
      nameField,
      costField,
      descriptionField,
      Container(
        margin: EdgeInsets.symmetric(vertical: 30),
        child: createExpenseButton,
      ),
    ];

    if (widget.showRemove) {
      children.add(Container(
        child: removeExpenseButton,
      ));
    }

    return Form(
      key: _formKey,
      child: Flexible(
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: ListView(
            children: children,
          ),
        ),
      ),
    );
  }
}
