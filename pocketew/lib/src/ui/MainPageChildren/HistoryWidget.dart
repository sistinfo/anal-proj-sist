import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pocketew/src/models/Expense.dart';
import 'package:pocketew/src/services/ExpensesService.dart';
import 'package:pocketew/src/ui/MainPage.dart';
import 'package:pocketew/src/ui/MainPageChildren/ExpensesWidget.dart';
import 'package:pocketew/src/models/TypeExpense.dart';


class HistoryWidget extends StatelessWidget {
  final TypeExpense type;

  HistoryWidget({ @required this.type });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        HistoryListView(
          type: type,
        ),
      ],
    );
  }
}

class HistoryListView extends StatelessWidget {
  final dateFormat = DateFormat('dd/MM/yyyy');
  final ExpensesService service = ExpensesService();
  final TypeExpense type;

  HistoryListView({ @required this.type });

  Widget createItem(BuildContext context, Expense expense, TextStyle style) {
    return ListTile(
      onTap: () {
        if (type == TypeExpense.PERSONAL) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (ctxt) => ExpensesDetail(
              expense: expense,
              user: InheritedUserState.of(context).user,
            )),
          );
        }
      },
      leading: Text(dateFormat.format(DateTime.fromMillisecondsSinceEpoch(expense.time))),
      title: Text(
        expense.name,
        style: style,
      ),
      subtitle: Text(expense.ownerInfo.name),
      trailing: Text('R\$ ${expense.cost.toString()}'),
    );
  }

  @override
  Widget build(BuildContext context) {
    final style = TextStyle(
      fontSize: 18,
    );
    String id;
    if (type == TypeExpense.GROUP) {
      id = InheritedUserState.of(context).user.idGroup;
      if (id == null) {
        return Flexible(
          child: Center(
            child: Text(
              'Entre em um grupo para carregar as despesas.',
              textAlign: TextAlign.center,
              style: style,
            ),
          ),
        );
      }
    } else {
      id = InheritedUserState.of(context).user.id;
    }
    return Flexible(
      child: FutureBuilder(
        future: service.getAll(id, type: type, reverse: true),
        builder: (BuildContext ctxt, AsyncSnapshot<List<Expense>> snap) {
          if (snap.hasData) {
            if (snap.data.isEmpty) {
              return Center(
                child: Text('Não há despesas cadastradas.', style: style),
              );
            }
            return ListView.builder(
              itemCount: snap.data.length,
              itemBuilder: (BuildContext itemCtxt, index) => createItem(context, snap.data[index], style),
              padding: EdgeInsets.all(12),
            );
          } else {
            return Center(
              child: Image.asset('assets/loading.gif'),
            );
          }
        },
      )
    );
  }
}

class ExpensesDetail extends StatelessWidget {
  final Expense expense;
  final user;
  ExpensesDetail({ @required this.expense, Key key, this.user }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Atualizando uma despesa'),
      ),
      body: Column(
        children: <Widget>[
          ExpensesForm(
            buttonText: 'Atualizar',
            showRemove: true,
            value: expense,
            user: user,
          ),
        ],
      ),
    );
  }
}