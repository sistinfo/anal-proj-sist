import 'package:flutter/material.dart';
import 'package:pocketew/src/models/Expense.dart';
import 'package:pocketew/src/models/TypeExpense.dart';
import 'package:pocketew/src/services/ExpensesService.dart';
import 'package:pocketew/src/services/Helpers.dart';
import 'package:pocketew/src/ui/MainPage.dart';
import 'package:pocketew/src/ui/MainPageChildren/ReportsWidgetChildren/ReportsCharts.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ReportsWidget extends StatelessWidget {
  final expensesServices = ExpensesService();
  final TypeExpense type;
  ReportsWidget({ this.type });

  @override
  Widget build(BuildContext context) {
    final _style = TextStyle(
      fontSize: 18,
      color: Colors.white,
      fontWeight: FontWeight.bold
    );
    var id;
    final user = InheritedUserState.of(context).user;
    if (user.idGroup == null && type == TypeExpense.GROUP) {
      return Center(
        child: Text(
          'Entre em algum grupo para visualizar esse conteúdo.'
        ),
      );
    } else if (type == TypeExpense.PERSONAL) {
      id = user.id;
    } else {
      id = user.idGroup;
    }
    final children = [
      Card(
        color: Color(0xff01A0C7),
        child: InkWell(
          onTap: () async {
            try {
              showLoadingDialog(context);
              final data = await expensesServices.monthlyExpenses(id, type: type);
              Navigator.pop(context);
              Navigator.push(
                context, MaterialPageRoute(
                  builder: (context) => TimeSeriesChart(data)
              ),
              );
            } catch(error) {
              print(error);
              Navigator.pop(context);
              showAlertDialog('Não foi possível retornar os dados.', context);
            }
          },
          child: Center(
            child: Text('Gastos Mensais', style: _style),
          ),
        ),
      ),
      Card(
        color: Color(0xff01A0C7),
        child: InkWell(
          onTap: () async {
            try {
              showLoadingDialog(context);
              final data = await expensesServices.yearlyExpenses(id, type: type);
              Navigator.pop(context);
              Navigator.push(
                context, MaterialPageRoute(
                  builder: (context) => TimeSeriesChart(data)
              ),
              );
            } catch(error) {
              print(error);
              Navigator.pop(context);
              showAlertDialog('Não foi possível retornar os dados.', context);
            }
          },
          child: Center(
            child: Text('Gastos Anuais', style: _style),
          ),
        ),
      ),
    ];
    return GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      children: children,
    );
  }
}