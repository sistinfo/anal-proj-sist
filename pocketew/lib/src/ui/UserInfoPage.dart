import 'package:flutter/material.dart';
import 'package:pocketew/src/models/User.dart';
import 'package:pocketew/src/services/Helpers.dart';
import 'package:pocketew/src/services/UsersService.dart';
import 'MainPage.dart';

class UserInfoPage extends StatefulWidget {
  final User user;
  final onUserChange;
  final usersService = UsersService();
  UserInfoPage({ Key key, @required this.user, this.onUserChange }) : super(key : key);

  @override
  _UserInfoPageState  createState() => _UserInfoPageState();
}

class _UserInfoPageState extends State<UserInfoPage> {
  TextStyle _style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordController = TextEditingController();
  TextEditingController _emailController;
  TextEditingController _nameController;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController(text: widget.user.id);
    _nameController = TextEditingController(text: widget.user.name);
  }

  @override
  Widget build(BuildContext context) {
    final nameField = TextFormField(
      controller: _nameController,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Nome',
      ),
      validator: (value) {
        if (value.length <= 3) {
          return 'O nome deve ter mais de 3 caracteres';
        }
        return null;
      },
    );
    final emailField = TextFormField(
      controller: _emailController,
      enabled: false,
      obscureText: false,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Email',
      ),
    );
    final passwordField = TextFormField(
      controller: _passwordController,
      obscureText: true,
      style: _style,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: 'Senha',
      ),
      validator: (value) {
        if (value.isNotEmpty && value.length < 6) {
          return 'A senha deve ter no mínimo 6 caracteres';
        }
        return null;
      },
    );
    final createAccountButton = Material(
      elevation: 5.0,
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          if (_formKey.currentState.validate()) {
            try {
              showLoadingDialog(context);
              final newUser = User(
                name: _nameController.text,
                id: _emailController.text,
                password: _passwordController.text,
              );
              await widget.usersService.update(newUser);
              if (widget.onUserChange != null) {
                widget.onUserChange();
              }
              Navigator.pop(context);
              showAlertDialog('Sucesso!', context);
            } catch(error) {
              Navigator.pop(context);
              showAlertDialog('Não foi possível atualizar o usuário.', context);
            }
          }
        },
        child: Text('Atualizar',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );
    final deleteAccountButton = Material(
      elevation: 5.0,
      color: Colors.red,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          try {
            showLoadingDialog(context);
            await widget.usersService.delete(widget.user.id);
            Navigator.of(context).popUntil((route) => route.isFirst);
          } catch (error) {
            Navigator.pop(context);
            showAlertDialog('Não foi possível remover sua conta', context);
          }
        },
        child: Text('Remover Conta',
            textAlign: TextAlign.center,
            style: _style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Alterar informações pessoais'),
      ),
      body: Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(36.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: nameField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: emailField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  child: passwordField,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30.0),
                  child: createAccountButton,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 0.0),
                  child: deleteAccountButton,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
